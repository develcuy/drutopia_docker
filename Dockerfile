FROM ubuntu:14.04

WORKDIR /opt/drutopia

# Set project name here
ENV PROJECT_NAME="myproject"

# Set the Drush version.
ENV DRUSH_VERSION 8.*

# Install PHP, git, curl
RUN apt-get update
RUN yes | apt-get install git curl php5 php5-gd php5-curl php5-mysql

## Install composer, drush
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer global require drush/drush:"$DRUSH_VERSION" --prefer-dist

# Setup project working directory
RUN mkdir /opt/drutopia/$PROJECT_NAME 
COPY . /opt/drutopia/$PROJECT_NAME
WORKDIR /opt/drutopia/$PROJECT_NAME

# Create the project
RUN composer create-project drupal/drupal $PROJECT_NAME ~8.2.6

# Initial composer.json is currently mounted for development of the Dockerfile.
# Fetch Drutopia composer.json from ronaldmulero repo
# RUN curl https://gitlab.com/ronaldmulero/drutopia_composer/raw/master/composer.json > composer.json
RUN composer update

# Install Drupal Console
RUN curl https://drupalconsole.com/installer -L -o drupal.phar && mv drupal.phar /usr/local/bin/drupal && chmod +x /usr/local/bin/drupal
RUN composer require drupal/console:~1.0 --prefer-dist --optimize-autoloader
